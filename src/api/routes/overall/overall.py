from fastapi.routing import APIRouter
from fastapi.security import OAuth2PasswordRequestForm
from fastapi import Depends, HTTPException, status
from api.authenticator import authenticator
from tools.position_scopes_map import get_position_scopes_map
from api.schemas import Token


router = APIRouter()


@router.post("/login", response_model=Token)
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    """
        Login to system by credentials from form_data.
    """
    user = authenticator.authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect username or password")
    if user.position == authenticator.ADMIN_POSITION:
        scopes = []
    else:
        scopes = get_position_scopes_map().get(user.position)
    access_token = authenticator.create_access_token(data={"sub": user.username, "scopes": scopes})

    return {"access_token": access_token, "token_type": "bearer"}