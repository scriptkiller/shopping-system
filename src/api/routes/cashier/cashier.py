from fastapi.routing import APIRouter
from api.repository import repository
from api.schemas import OrderIDSchema, BillSchema, MessageResponse
from fastapi import HTTPException, status, Security
from api.authenticator import authenticator
from auth.models import StaffInDb


router = APIRouter()


@router.post("/order", response_model=OrderIDSchema)
async def create_order(
        product: str,
        current_user: StaffInDb = Security(authenticator.get_current_user, scopes=['cashier'])
) -> OrderIDSchema:
    """
    Create order with {product} item. Work only for Cashier profile
    """
    product_id = repository.find_product_by_name(product)
    if product_id:
        return OrderIDSchema(order_id=repository.add_order(product_id))
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Product doesn`t exist")


@router.get("/bill/{order_id}", response_model=BillSchema)
async def get_bill(
        order_id: int,
        current_user: StaffInDb = Security(authenticator.get_current_user, scopes=['cashier'])
) -> BillSchema:
    """
    Return bill for {order_id}. Work only for Cashier profile
    """
    bill = repository.get_bill(order_id)
    if not bill:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Order doesn`t processed or doesn`t exist")
    return bill


@router.patch("/bill/{order_id}", response_model=MessageResponse)
async def order_payed(
        order_id: int,
        current_user: StaffInDb = Security(authenticator.get_current_user, scopes=['cashier'])
) -> MessageResponse:
    """
        Set status PAID for bill by {order_id}. Work only for Cashier profile
    """
    if repository.order_payed(order_id):
        return MessageResponse(message="order was payed.")
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                        detail="Order doesn`t processed or doesn`t exist")
