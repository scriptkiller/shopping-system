from fastapi.routing import APIRouter
from auth.models import StaffInDb
from enum import Enum
from fastapi import Security, HTTPException, status
from api.authenticator import authenticator
from api.repository import repository
from tools.position_ids_map import get_position_ids_map
from api.schemas import MessageResponse


router = APIRouter()


class Positions(Enum):
    CASHIER = 'cashier'
    ACCOUNTANT = 'accountant'
    SELLER = 'seller'


@router.post('/staff', response_model=MessageResponse)
async def create_staff(
        username: str,
        password: str,
        position: Positions,
        current_user: StaffInDb = Security(authenticator.get_current_user)
):
    """
        Create staff account with {position} type. Work only for Admin profile
    """
    if current_user.position != authenticator.ADMIN_POSITION:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not enough permissions",
            headers={"WWW-Authenticate": "Bearer"},
        )
    if repository.create_new_staff(
            username,
            authenticator.get_password_hash(password),
            get_position_ids_map()[str(position.value).lower()]
    ):
        return MessageResponse(message="staff account was created")
    else:
        return MessageResponse(message="staff account already exist")