from .cashier import router as cashier_router
from .seller import router as seller_router
from .accountant import router as accountant_router
from .overall import router as overall_router
from .admin import router as admin_router