from pydantic import BaseModel
from datetime import datetime


class BillSchema(BaseModel):
    name: str
    price: str
    order_created_at: datetime
    bill_created_at: datetime

    class Config:
        schema_extra = {
            "example": {
                "name": "IPHONE XR",
                "price": "$ 12345.00",
                "order_created_at": "2020-10-26T14:58:59",
                "bill_created_at": "2020-10-27T13:58:47.845899"
            }
        }


