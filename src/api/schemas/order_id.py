from pydantic import BaseModel


class OrderIDSchema(BaseModel):
    order_id: int

    class Config:
        schema_extra = {
            "example": {
                "order_id": 1
            }
        }
