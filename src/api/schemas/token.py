from pydantic import BaseModel


class Token(BaseModel):
    access_token: str
    token_type: str

    class Config:
        schema_extra = {
            "example": {
                "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6W10sImV4cCI6MTYwMzgzNzMxNH0.w10paBr2_0wF0QNBPCIz2OPYcdUndYEzeQsiU-VecMw",
                "token_type": "bearer"
            }
        }