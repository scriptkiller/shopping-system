from sqlalchemy import Column
from sqlalchemy.dialects.mysql import VARCHAR, BIGINT

from .base import Base
from .mixins import JSONSerializable, MysqlPrimaryKeyMixin, MysqlTimestampsMixin


class Product(Base, JSONSerializable, MysqlPrimaryKeyMixin, MysqlTimestampsMixin):
    __tablename__ = "products"

    name = Column(VARCHAR(length=255), nullable=False, unique=True)
    price_cents = Column(BIGINT(unsigned=True), nullable=False)
