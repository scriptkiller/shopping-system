from sqlalchemy import Column, UniqueConstraint
from sqlalchemy.dialects.mysql import VARCHAR

from .base import Base
from .mixins import JSONSerializable, MysqlPrimaryKeyMixin, MysqlTimestampsMixin, MysqlStaffPositions


class Staff(Base, JSONSerializable, MysqlPrimaryKeyMixin, MysqlTimestampsMixin, MysqlStaffPositions):
    __tablename__ = "staff"

    username = Column(VARCHAR(length=255), nullable=False)
    password = Column(VARCHAR(length=64), nullable=False)

    UniqueConstraint('username', 'password', name='uix_user_pass')
