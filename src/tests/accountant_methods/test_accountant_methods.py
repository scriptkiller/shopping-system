import pytest
from api.repository import repository
from database.models.mixins import MysqlStaffPositions
from api.authenticator import authenticator
from fastapi import status


@pytest.fixture
def create_accountant():
    is_staff_created = repository.create_new_staff('testaccountant',
                                                   authenticator.get_password_hash('testaccountant'),
                                                   MysqlStaffPositions.POSITION_ACCOUNTANT)
    assert is_staff_created is True
    yield
    repository.delete_staff('testaccountant')


@pytest.mark.accountant
def test_get_orders(client, create_accountant):
    response_login = client.post('/login',
                                 headers={"Content-Type": "application/x-www-form-urlencoded"},
                                 data={"username": "testaccountant",
                                       "password": "testaccountant",
                                       "grant_type": "",
                                       "scope": "",
                                       "client_id": "",
                                       "client_secret": ""})
    assert response_login.status_code == status.HTTP_200_OK
    access_token = response_login.json()['access_token']
    auth_header = {"Authorization": f"Bearer {access_token}"}
    response_all_orders = client.get('/all-orders?date_from=2020-08-10&date_to=2020-10-10',
                                     headers=auth_header)
    assert response_all_orders.status_code == status.HTTP_200_OK


@pytest.mark.seller
def test_validation(client, create_accountant):
    response_login = client.post('/login',
                                 headers={"Content-Type": "application/x-www-form-urlencoded"},
                                 data={"username": "testaccountant",
                                       "password": "testaccountant",
                                       "grant_type": "",
                                       "scope": "",
                                       "client_id": "",
                                       "client_secret": ""})

    assert response_login.status_code == status.HTTP_200_OK
    access_token = response_login.json()['access_token']
    auth_header = {"Authorization": f"Bearer {access_token}"}
    response_all_orders = client.get('/all-orders?date_from=s&date_to=s',
                                     headers=auth_header)
    assert response_all_orders.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    response_all_orders = client.get('/all-orders?date_from=2020-04-10%2012%3A54%3A00&date_to=2020-08-10%2012%3A54%3A00',
                                     headers=auth_header)
    assert response_all_orders.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.accountant
def test_permission_accountant(client, create_accountant):
    response_login = client.post('/login',
                                 headers={"Content-Type": "application/x-www-form-urlencoded"},
                                 data={"username": "testaccountant",
                                       "password": "testaccountant",
                                       "grant_type": "",
                                       "scope": "",
                                       "client_id": "",
                                       "client_secret": ""})

    assert response_login.status_code == status.HTTP_200_OK
    access_token = response_login.json()['access_token']
    auth_header = {"Authorization": f"Bearer {access_token}"}
    response_bill = client.get(f'/bill/1',
                               headers=auth_header)
    assert response_bill.status_code == status.HTTP_401_UNAUTHORIZED
    assert response_bill.json() == {"detail": "Not enough permissions"}

    response_all_orders = client.get(f'/orders-np',
                                     headers=auth_header)
    assert response_all_orders.status_code == status.HTTP_401_UNAUTHORIZED
    assert response_all_orders.json() == {"detail": "Not enough permissions"}
