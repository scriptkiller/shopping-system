import pytest
from api.repository import repository
from database.models.mixins import MysqlStaffPositions
from api.authenticator import authenticator
from fastapi import status


@pytest.fixture
def create_seller():
    is_staff_created = repository.create_new_staff('testseller',
                                                   authenticator.get_password_hash('testseller'),
                                                   MysqlStaffPositions.POSITION_SHOP_ASSISTANT)
    assert is_staff_created is True
    yield
    repository.delete_staff('testseller')


@pytest.fixture
def add_product():
    repository.add_product({'name': 'Test Product', 'price_cents': 1290000})
    yield
    repository.delete_product('Test Product')


@pytest.fixture
def order_id(add_product):
    product_id = repository.find_product_by_name('Test Product')
    assert product_id is not None
    order_id = repository.add_order(product_id)
    yield order_id
    repository.delete_order(order_id)


@pytest.mark.seller
def test_processed_order(client, create_seller, order_id):
    response_login = client.post('/login',
                                 headers={"Content-Type": "application/x-www-form-urlencoded"},
                                 data={"username": "testseller",
                                       "password": "testseller",
                                       "grant_type": "",
                                       "scope": "",
                                       "client_id": "",
                                       "client_secret": ""})

    assert response_login.status_code == status.HTTP_200_OK
    access_token = response_login.json()['access_token']
    auth_header = {"Authorization": f"Bearer {access_token}"}
    response_np_orders = client.get('/orders-np',
                                    headers=auth_header)
    assert response_np_orders.status_code == status.HTTP_200_OK
    is_order_id_exist = False
    for order in response_np_orders.json()['orders']:
        if order['order_id'] == order_id:
            is_order_id_exist = True
            break
    assert is_order_id_exist
    response_order_processed = client.patch(f'/order/{order_id}',
                                            headers=auth_header)
    assert response_order_processed.status_code == status.HTTP_200_OK
    assert response_order_processed.json() == {"message": "order has been processed"}
    response_np_orders = client.get('/orders-np',
                                    headers=auth_header)
    is_order_id_exist = False
    for order in response_np_orders.json()['orders']:
        if order['order_id'] == order_id:
            is_order_id_exist = True
            break
    assert is_order_id_exist is False


@pytest.mark.seller
def test_process_not_exist_order(client, create_seller):
    response_login = client.post('/login',
                                 headers={"Content-Type": "application/x-www-form-urlencoded"},
                                 data={"username": "testseller",
                                       "password": "testseller",
                                       "grant_type": "",
                                       "scope": "",
                                       "client_id": "",
                                       "client_secret": ""})

    assert response_login.status_code == status.HTTP_200_OK
    access_token = response_login.json()['access_token']
    auth_header = {"Authorization": f"Bearer {access_token}"}
    response_process_order = client.patch('/order/0',
                                          headers=auth_header)
    assert response_process_order.status_code == status.HTTP_404_NOT_FOUND
    assert response_process_order.json() == {"detail": "Order not found, or already has been processed"}


@pytest.mark.seller
def test_permission_seller(client, create_seller):
    response_login = client.post('/login',
                                 headers={"Content-Type": "application/x-www-form-urlencoded"},
                                 data={"username": "testseller",
                                       "password": "testseller",
                                       "grant_type": "",
                                       "scope": "",
                                       "client_id": "",
                                       "client_secret": ""})

    assert response_login.status_code == status.HTTP_200_OK
    access_token = response_login.json()['access_token']
    auth_header = {"Authorization": f"Bearer {access_token}"}
    response_bill = client.get(f'/bill/1',
                               headers=auth_header)
    assert response_bill.status_code == status.HTTP_401_UNAUTHORIZED
    assert response_bill.json() == {"detail": "Not enough permissions"}

    response_all_orders = client.get(f'/all-orders?date_from=2020-10-24&date_to=2020-10-28',
                                     headers=auth_header)
    assert response_all_orders.status_code == status.HTTP_401_UNAUTHORIZED
    assert response_all_orders.json() == {"detail": "Not enough permissions"}