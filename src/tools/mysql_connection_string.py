import settings


def mysql_connection_string() -> str:
    return "mysql+mysqldb://{}:{}@{}:{}/{}?charset=utf8mb4".format(
        settings.MYSQL_USER,
        settings.MYSQL_PASSWORD,
        settings.MYSQL_HOST,
        settings.MYSQL_PORT,
        settings.MYSQL_DATABASE
    )