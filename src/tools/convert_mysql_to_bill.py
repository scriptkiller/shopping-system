from api.schemas import BillSchema
from tools.get_price_with_sale import get_price_with_sale
from datetime import datetime


def convert_mysql_to_bill(row, bill_created_at: datetime) -> BillSchema:
    price_str = get_price_with_sale(row.price_cents, row.product_created_at)
    return BillSchema(name=row.name,
                      price=price_str,
                      order_created_at=row.order_created_at,
                      bill_created_at=bill_created_at)
