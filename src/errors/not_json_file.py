class NotJSONFileError(Exception):
    def __init__(self):
        super().__init__("File must be in format .json")
